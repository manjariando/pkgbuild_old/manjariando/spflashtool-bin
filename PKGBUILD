# Maintainer: tioguda <guda.flavio@gmail.com>
# Contributor: Omar Pakker <archlinux@opakker.nl>

_pkgname=spflashtool
pkgname=${_pkgname}-bin
pkgver=5.2136
pkgrel=1
pkgdesc="SP Flash Tool is an application to flash your MediaTek (MTK) SmartPhone."
arch=('i686' 'x86_64')
url="https://spflashtools.com/category/linux"
license=('GPL-3.0')
makedepends=('gendesk' 'imagemagick')
provides=("${_pkgname}")
conflicts=("${_pkgname}")
options=('!strip')
source=("https://spflashtools.com/wp-content/uploads/SP_Flash_Tool_v${pkgver}_Linux.zip"
        "https://metainfo.manjariando.com.br/${_pkgname}/com.${_pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${_pkgname}/${_pkgname}.png"
        'https://www.gnu.org/licenses/gpl-3.0.txt'
        "60-${_pkgname}.rules")
sha256sums=('e2a506957ce6039b25706e56490535c185f59e463f85072fdaa13ca5403a8283'
            'd83fd0e4faae83c5612f16d2556138dafd04fc640a00d33e98047003a297123c'
            'fe0b9c1de77c687623bfc07733041d1387f755493cdf904e6afcb47f784d34c7'
            '3972dc9744f6499f0f9b2dbf76696f2ae7ad8af9b23dde66d6af86c9dfb36986'
            'a46a4fc667cf5d6114f3757dc8dbc6cfbc27229319d48f6d78c1e026b34210da')

# Workaround for source file download, which requires the 'Referer' header to be set
DLAGENTS=('http::/usr/bin/curl -fLC - --retry 3 --retry-delay 3 -e %u -o %o %u'
          "${DLAGENTS[@]}")

prepare() {
   # Create exec file. Required for the binary to find its own .so files
    {
        echo '#!/bin/sh'
        echo 'export LD_LIBRARY_PATH="/opt/'"${_pkgname}"'"'
        echo '/opt/'"${_pkgname}"'/flash_tool "${@}"'
    } > "${srcdir}/${_pkgname}"
}

_spflashtool_desktop="[Desktop Entry]
Version=1.0
Name=SP Flash Tool
Type=Application
Comment=SP Flash Tool is an application to flash your MediaTek (MTK) SmartPhone
Comment[pt_BR]=SP Flash Tool é um aplicativo para flash seu SmartPhone MediaTek (MTK)
Exec=/usr/bin/${_pkgname}
Icon=${_pkgname}
Categories=Development;"

build() {
    cd "${srcdir}"
    echo -e "$_spflashtool_desktop" | tee com.${_pkgname}.desktop
}

package() {
    depends=('libpng12' 'qtwebkit')

    local folderName="SP_Flash_Tool_v${pkgver}_Linux"

    # Clean files we do not need
    rm "${srcdir}/${folderName}/flash_tool.sh"
    rm -r "${srcdir}/${folderName}/bin"
    rm -r "${srcdir}/${folderName}/lib"
    rm -r "${srcdir}/${folderName}/plugins"
    rm -r "${srcdir}/${folderName}/Driver"

    # Install remaining files
    install -Dm644 -t "${pkgdir}/opt/${_pkgname}/" "${srcdir}/${folderName}/"*

    # Mark the binary as executable and install the shell file created in prepare()
    chmod +x "${pkgdir}/opt/${_pkgname}/flash_tool"
    install -Dm755 "${srcdir}/${_pkgname}" "${pkgdir}/usr/bin/${_pkgname}"

    # Appstream
    install -Dm644 "${srcdir}/com.${_pkgname}.desktop" "${pkgdir}/usr/share/applications/com.${_pkgname}.desktop"
    install -Dm644 "${srcdir}/com.${_pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${_pkgname}.metainfo.xml"
    install -Dm644 "${srcdir}/gpl-3.0.txt" "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"

    for size in 22 24 32 48 64 128; do
        mkdir -p "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
        convert "${srcdir}/${_pkgname}.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/${_pkgname}.png"
    done

    # Udev rule
    install -Dm644 "${srcdir}/60-${_pkgname}.rules" "${pkgdir}/usr/lib/udev/rules.d/60-${_pkgname}.rules"

    # Link QT assistant so the help menu works
    install -dm755 "${pkgdir}/opt/${_pkgname}/bin"
    ln -sf "/usr/lib/qt4/bin/assistant" "${pkgdir}/opt/${_pkgname}/bin/assistant"
}
